@extends('layouts.app')

@section('content')

<div class="row">
	<div class="col-8 offset-2">
		<h3>Add more assets</h3>
	  	<p>Click on the Tabs to display the forms.</p>
	   	<div class="nav nav-pills bg-light">
	   	<div class="nav navbar-light nav-tabs bg-light">
	   		<a class="nav-link active" data-toggle="tab" href="#addCategory" >Add Category</a>
	   		<a class="nav-link" data-toggle="tab" href="#addAsset" href="">Add Asset</a>
	   	</div>
	   	<div class="tab-content bg-white">
		   	<div id="addCategory" class="tab-pane active"><br>
				<div id="addCatNotif" class="card-body"></div>
				<form>
					<div class="form-group">
						<label for="name">Category name: </label>

						<input class="form-control" type="text" id="catName" name="category">
					</div>
				</form>
				<button class="btn btn-success float-right" id="addCatBtn">Add Category</button>

					<div class="form-group">
						<label for="category_id">Category: </label>
						<select class="form-control" id="category_id" name="category">
							<option>Select a category:</option>
							@if(count($categories) > 0)
								@foreach($categories as $category)
									<option value="{{$category->id}}">{{$category->name}}</option>
								@endforeach
							@endif
						</select>
					</div>
		   	</div>   	
		   	<div id="addAsset" class="tab-pane fade">
		   		<div class="card-body"></div>
   				@if ($errors->any())
   				    <div class="alert alert-danger">
   				        <ul>
   				            @foreach ($errors->all() as $error)
   				                <li>{{ $error }}</li>
   				            @endforeach
   				        </ul>
   				    </div>
   				@endif

				<form method="POST" action="/products" enctype="multipart/form-data">
					@csrf
					<div class="form-group">
						<label for="name">Name: </label>
						<input class="form-control" type="text" name="name" id="name">
					</div>
					<div class="form-group">
						<label for="description">Description: </label>
						<input class="form-control" type="text" name="description" id="description">
					</div>
					<div class="form-group">
						<label for="category_id">Category: </label>
						<select class="form-control" id="category_id" name="category">
							<option>Select a category:</option>
							@if(count($categories) > 0)
								@foreach($categories as $category)
									<option value="{{$category->id}}">{{$category->name}}</option>
								@endforeach
							@endif
						</select>
					</div> --}}
					<div class="form-group">
						<label for="image">Image:</label>
						<input class="form-control" type="file" name="image" id="image">
					</div>

					<button type="submit" class="btn btn-success">
						Add Product
					</button>							
				</form>
		   	</div>
		   	
		</div>
	</div>
</div>
<script src="{{asset('js/addCat.js')}}"></script>
@endsection