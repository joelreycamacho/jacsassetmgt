@extends('layouts.app')

@section('content')
	<div class="jumbotron bg-info"></div>
	<div class="jumbotron m-5 text-center bg-info">	        
	        <h4>JAC's Asset Management System</h4>
	        <p>Please log in to learn more about our assets.</p>
	        <hr>
	        @php($year = date('Y'))  
	        <small>
	        	<p>&copy {{$year}} JAC Builders, Inc. All Rights Reserved. Developed and maintained by Joelrey Camacho.</p>
	        </small>
    </div>
@endsection
