@extends('layouts.app')

@section('content')
	<div class="row">
		<div class="col-10 offset-1">
			@if ($errors->any())
			    <div class="alert alert-danger">
			        <ul>
			            @foreach ($errors->all() as $error)
			                <li>{{ $error }}</li>
			            @endforeach
			        </ul>
			    </div>
			@endif
			
			<div class="bg-light rounded">
				<div class="card-header p-0 pt-2 pl-3">
					<div class="row">
						<div class="col-6">
							<h4 class="">Assets</h4>
						</div>
						<div class="col-6">
							<h4 class="text-right mr-3"><a class="btn btn-success" href="/assets/create">Add more</a></h4>
						</div>
					</div>
				</div>	
				<div class="card-body">
					<div class="row">		
						@foreach($categories as $category)
							<div class="col-6">
							<h4>{{$category->name}}</h4>
							<table class="table table-info table-responsive table-sm">
								<thead>
									<tr class="">	
										<th>Serial Number</th>
										<th>Status</th>
										<th>Create Date</th>
									</tr>
								</thead>

								<tbody>
								
								@php($count=0)
								@foreach($assets as $asset)	
																		
									@if($asset->category_id == $category->id)
									<tr>
										<td>{{$asset->serialNum}}</td>
										<td>
											@if($asset->isAvailable == 1)
													Available
											@else
													Unavailable
											@endif
										</td>
										<td>{{$asset->created_at}}</td>
									</tr>

									@php($count++)						
									@endif
								
								@endforeach
									<tr class="alert">
										<th>Total:</th>
										<th>{{$count}}</th>
										<th></th>
									</tr>
								</tbody>
							</table>
							</div>
						@endforeach
					</div>	
				</div>	
			</div>
		</div>
	</div>
@endsection
