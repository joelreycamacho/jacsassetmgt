@extends('layouts.app')

@section('content')
<div class="row">
	<div class="col-10 offset-1">
		<div class="row">
			<div class="col-6 offset-3">
				@if ($errors->any())
				    <div class="alert alert-danger p-0 pt-3 m-0">
				        <ul>
				            @foreach ($errors->all() as $error)
				                <li>{{ $error }}</li>
				            @endforeach
				        </ul>
				    </div>
				@endif
				@if(session('status'))
					<div class="p-1 alert alert-success text-center m-0" role="alert">
					  {{session('status')}}
					</div>
				@endif
			</div>
		</div>	
		<div class="card bg-light">
			<div class="card-header p-0 pt-2 pl-3">
				<h4 class="">Asset Lines</h4>
			</div>	
			<div class="card-body p-1 ">
				@foreach($categories as $category)
					<div class="row">
						<div class="col-md-4 p-1 ">
							<img class="w-100 pl-3" src="{{$category->img_path}}">
						</div>
						<div class="col-md-4 pl-2">
							@php($count=0)
							@foreach($assets as $asset)												
								@if($asset->category_id == $category->id && $asset->isAvailable == 1)
									@php($count++)						
								@endif					
							@endforeach
							@if($category->isActive == 1)
								@php($status = "active")
							@else
								@php($status = "inactive")
							@endif
							<h5>{{$category->name}}</h5>
							<h6 class="">
							@can('isAdmin')	
								@if($status == "active")
									<span id="" 
										class="alert-primary p-1 rounded">Active</span>
								@else
									<span id="spanInactive{{$category->id}}"
										class="alert-warning p-1 rounded">Inactive</span>
								@endif
							@endcan
								@if($count > 0) 
									<span id=""
										class="alert-success p-1 rounded">Available: <b class="badge badge-pill badge-success m-1">{{$count}}</b></span></h6>
								@else
									<span id=""
										class="alert-danger p-1 rounded">Unavailable</span>
								@endif
							</h6>
							<b class="">Asset Code: {{$category->code}}</b>
							<p>{{$category->description}}</p><br>	
						</div>
						<div class="col-md-4 p-2">
							@can('isAdmin')							
								<a href="/categories/{{$category->id}}/edit" class="btn btn-warning btn-action mt-3">Edit</a>
								<form method="POST" action="/categories/{{$category->id}}">
									@csrf
									@method('DELETE')
									@if($status == "active")
										<button type="submit" class="btn btn-danger btn-action">Deactivate</button>
									@else
										<button type="submit" class="btn btn-success btn-action">Reactivate</button>
									@endif	 	
								</form>
							@else						
								@if($count > 0 && $status == "active")					
									<button class="btn btn-primary mt-2" data-toggle="modal" data-target="#rform{{$category->id}}">Request Deployment</button>			
									<!-- Modal -->
									<div class="modal fade" id="rform{{$category->id}}" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
									  	<div class="modal-dialog modal-dialog-centered" role="document">
									    	<div class="modal-content">
									      		<div class="modal-header p-3">
									        		<h5 class="modal-title" id="exampleModalLongTitle">Request for deployment of <b>{{$category->name}}</b></h5>
									        		<button type="button" class="close btn btn-outline-warning" data-dismiss="modal" aria-label="Close">
									          			<span class="" aria-hidden="true">&times;</span>
									        			</button>
									      		</div>
									      		@if ($errors->any())
									      		    <div class="alert alert-danger p-0 pt-3 m-0">
									      		        <ul>
									      		            @foreach ($errors->all() as $error)
									      		                <li>{{ $error }}</li>
									      		            @endforeach
									      		        </ul>
									      		    </div>
									      		@endif
									      		@if(session('status'))
									      			<div class="p-1 alert alert-success text-center m-0" role="alert">
									      			  {{session('status')}}
									      			</div>
									      		@endif
									      		<div class="modal-body">
									        		<form method="POST" action="/transactions" class="" 
									        			enctype="multipart/form-data" >
									        			@csrf
									        			<input hidden type="text" name="category_id" value="{{$category->id}}">
									        			<div class="form-group-row p-0">
									        				<label class="col-md-4 col-form-label">Deploy date:</label>
									        				<input class="form-control-sm col-md-7" type="date" name="deployDate" value="{{$minDate}}"
									        				min ="{{$minDate}}" max="{{$maxDate}}">					        				 
									        			</div>
									        			<div class="form-group-row p-0">
									        				<label class="col-md-4 col-form-label">Return date:</label>
									        				<input class="form-control-sm col-md-7" type="date" name="returnDate" min ="{{$minDate}}" max="{{$maxDate}}">
									        			</div>
									        			<div class="text-right p-2">
									        				<button type="button" class="btn btn-warning" data-dismiss="modal">Cancel</button>
									        				<button type="submit" class="btn btn-primary mr-4">Submit</button>
									        			</div>
									        		</form>
									      		</div>
									    	</div>
									  	</div>
									</div>
								@endif
							@endcan		
						</div>
					</div>
					<hr class="mt-2 mb-2">
				@endforeach	
			</div>
		</div>	
	</div>
</div>
@endsection