@extends('layouts.app')

@section('content')

<div class="row ">
	<div class="col-10 offset-1">

		<div class="card-header p-0 pt-2 pl-3 pr-3 rounded">
			<h4>Add more asset lines</h4>
		</div>	

	   	<div class="row card-body p-0 pt-1 pl-3 pr-3">	   		
		   	<div id="addCategory" class="col-6 alert-primary">
		   		<h5>Add Asset Line</h5>
				<div id="addCatNotif" class=""></div>
				<form>
					<div class="form-group">
						<label for="name">Category Name</label>
						<input class="form-control" type="text" id="catName" name="category">
					</div>

					<div class="form-group">
						<label for="description">Description</label>
						<textarea class="form-control" type="text" name="description" id="description"></textarea> 
					</div>

					<div class="form-group">
						<label for="description">Asset Code</label>
						<input class="form-control" type="text" name="code" id="code">
					</div>

					<div class="form-group">
						<label for="image">Image</label>
						<input class="form-control" type="file" name="image" id="image">
					</div>
				</form>
				<div class="float-right">
					<a class="btn btn-warning" href="/categories">Cancel</a>
					<button class="btn btn-primary" id="addCatBtn">Add Asset Line</button>
				</div>
		   	</div>  

		   	
		   	<div id="addAsset" class="col-6 alert-info">
		   		<h5>Add Asset</h5>
   				@if ($errors->any())
   				    <div class="alert alert-danger">
   				        <ul>
   				            @foreach ($errors->all() as $error)
   				                <li>{{ $error }}</li>
   				            @endforeach
   				        </ul>
   				    </div>
   				@endif

				<form method="POST" action="/assets" enctype="multipart/form-data">
					@csrf
					<div class="form-group">
						<label for="category_id">Category: </label>
						<select class="form-control" id="category_id" name="category">
							<option>Select a category:</option>
							@if(count($categories) > 0)
								@foreach($categories as $category)
									<option value="{{$category->id}}">{{$category->name}}</option>
								@endforeach
							@endif
						</select>
					</div> 

					<div class="form-group">
						<label for="name">Quantity </label>
						<input class="form-control" type="number" max="10" min="1" name="quantity" id="quantity">
					</div>
						
					<div class="float-right">
						<a class="btn btn-warning" href="/categories">Cancel</a>
						<button type="submit" class="btn btn-info">Add Asset</button>
					</div>						
				</form>
		   	</div>
		</div>
	</div>
</div>
<script src="{{asset('js/addCat.js')}}"></script>
@endsection