@extends('layouts.app')

@section('content')
	<div class="row ">
		<div class="col-8 offset-2 alert-secondary rounded p-2">
			<div class="">
				@if ($errors->any())
				    <div class="alert alert-danger">
				        <ul>
				            @foreach ($errors->all() as $error)
				                <li>{{ $error }}</li>
				            @endforeach
				        </ul>
				    </div>
				@endif
				<div class="card-header p-0 pt-2 pl-3 pr-3 rounded">
					<h4>Edit Asset Line</h4>
				</div>

				<div class="card-body">
					<form method="POST" action="/categories/{{$category->id}}" enctype="multipart/form-data">
						@csrf
						@method('PUT')
						<div class="form-group">
							<label for="name">Asset Line </label>
							<input class="form-control" type="text" name="name" id="name" value="{{$category->name}}">
						</div>
						<div class="form-group">
							<label for="description">Description </label>
							<textarea class="form-control" type="text" name="description" id="description" rows="4" 
							>{{$category->description}}</textarea> 
						</div>
						<div class="form-group">
							<label for="price">Asset Code: </label>
							<input class="form-control" type="text" name="code" id="code" value="{{$category->code}}">
						</div>
						<div class="form-group">
							<label for="image">Image:</label>
							<input class="form-control" type="file" name="image" id="image">
						</div>

						<div class="float-right">
							<a class="btn btn-warning btn-action" href="/categories">Cancel</a>
							<button class="btn btn-success btn-action" type="submit" >Save</button>
						</div>	
					</form>
				</div>
			</div>
		</div>
	</div>
@endsection