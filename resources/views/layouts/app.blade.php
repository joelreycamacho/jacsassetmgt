<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link data-n-head="true" rel="icon" type="image/x-icon" href="{{asset('images/joelrey.jpg') }}">
    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'Laravel') }}</title>

    {{-- <script src="https://www.paypal.com/sdk/js?client-id=AbRpR3iFsF9nT_fLhZNXIeNoApEg0rDyS1ZSsLlYRxoQe1duDJZ1RWf6FyFfRUl3Ss8yaI2sg5hj0NqZ"></script> --}}

    <!-- Fonts -->
    <link rel="dns-prefetch" href="//fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css?family=Nunito" rel="stylesheet">

    <!-- Styles -->
    {{-- <link rel="stylesheet" href="https://www.w3schools.com/w3css/4/w3.css"> --}}
    
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css" integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">

    {{-- <link href="{{asset('fontawesome/css/all.css') }}" rel="stylesheet"> --}}
    <link href="{{asset('css/style.css') }}" rel="stylesheet">
</head>
<body class="bg-info" onload="startTime()">
    <div id="app">
        {{-- <nav class="navbar navbar-expand-md nav-tabs bg-dark shadow-sm"> --}}
        <nav class="navbar navbar-expand-lg bg-dark fixed-top shadow-sm p-1">
            <div class="container">
                <b ><a class="app-name nav-link" href="{{ url('/home') }}">
                    {{ config('app.name', 'Laravel') }}
                </a></b>
                <button class="navbar-toggler btn navbar-light" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="{{ __('Toggle navigation') }}">
                    <span class="navbar-toggler-icon"></span>
                </button>

                <div class="collapse navbar-collapse" id="navbarSupportedContent">
                    <!-- Left Side Of Navbar -->
                   <ul class="navbar-nav mr-auto">
                    @guest
                    @else
                        <li class="nav-menu ml-1 text-right">
                            <a class="nav-link btn-outline-primary btn-md ml-2 pr-2" href="/transactions">Transactions</a>
                        </li> 
                        <li class="nav-menu ml-1 text-right">
                            <a class="nav-link btn-outline-primary btn-md pr-2" href="/categories">Asset Lines</a>
                        </li>
                        @can('isAdmin')
                        <li class="nav-menu ml-1 text-right">
                            <a class="nav-link btn-outline-primary btn-md pr-2" href="/assets">Assets</a>
                        </li>
                        @endcan
                    @endguest
                    </ul>
                    <!-- Right Side Of Navbar -->
                    <ul class="navbar-nav ml-auto text-white">
                        <!-- Authentication Links -->
                        @guest
                            <li class="nav-item text-left">
                                <a class="nav-link btn btn-outline-primary m-1" href="{{ route('login') }}">{{ __('Login') }}</a>
                            </li>
                            @if (Route::has('register'))
                            <li class="nav-item text-left">
                                <a class="nav-link btn btn-outline-primary m-1" href="{{ route('register') }}">{{ __('Register') }}</a>
                            </li>
                            @endif
                        @else
                            @php($date = trim(date("l, j F Y")))
                            <li class="text-right ml-1"><span class=" pr-3 ">{{$date}}
                                </span></li>
                            <li class="text-right ml-1"><span class=" pr-3 " id="timer"></span>
                                </li>                                                     
                            <li class="nav-item dropdown text-right ml-1">
                                <a id="navbarDropdown" class="dropdown-toggle nav-link btn-outline-primary btn-md pr-3" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" v-pre>
                                    {{ Auth::user()->name}}<span class="caret"></span>
                                </a>

                                <div class="dropdown-menu dropdown-menu-right bg-primary p-0" aria-labelledby="navbarDropdown">
                                    <a class="dropdown-item" href="{{ route('logout') }}"
                                       onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();">
                                        {{ __('Logout') }}
                                    </a>

                                    <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                        @csrf
                                    </form>
                                </div>
                            </li>
                        @endguest
                    </ul>
                </div>
            </div>
        </nav>
        <br>
        <main class="py-4">
            <div class="container">
                @yield('content')
            </div>
        </main>
    </div>

    <script>
        function startTime() {
            var today = new Date();
            var h = today.getHours();
            var m = today.getMinutes();
            var s = today.getSeconds();
            if (h > 12) {
               h = h - 12
            }
            m = checkTime(m);
            s = checkTime(s);
            document.getElementById('timer').innerHTML =
            h + ":" + m + ":" + s;
            var t = setTimeout(startTime, 500);
        }

        function checkTime(i) {
          if (i < 10) {i = "0" + i};  // add zero in front of numbers < 10
          return i;
        }
    </script>

    <script src="https://code.jquery.com/jquery-3.4.1.slim.min.js" integrity="sha384-J6qa4849blE2+poT4WnyKhv5vZF5SrPo0iEjwBvKU7imGFAV0wwj1yYfoRSJoZ+n" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js" integrity="sha384-Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js" integrity="sha384-wfSDF2E50Y2D1uUdj0O3uMBJnjuUD4Ih7YwaYd1iqfktj0Uod8GCExl3Og8ifwB6" crossorigin="anonymous"></script>

</body>
</html>
