@extends('layouts.app')

@section('content')

<div class="row ">
	<div class="col-10 offset-1">

		<div class="card-header p-0 pt-2 pl-3 pr-3 rounded">
			<h4>Request Asset</h4>
		</div>	

	   	<div class="row card-body p-0 pt-1 pl-3 pr-3">	   		
		   	<div id="" class="col-6 alert-primary">
		   		<h5>Add Asset Line</h5>
				<div id="addCatNotif" class=""></div>
				<form>
					<div class="form-group">
						<label for="name">Category Name</label>
						<input class="form-control" type="text" id="catName" name="category">
					</div>

					<div class="form-group">
						<label for="description">Description</label>
						<textarea class="form-control" type="text" name="description" id="description"></textarea> 
					</div>

					<div class="form-group">
						<label for="description">Asset Code</label>
						<input class="form-control" type="text" name="code" id="code">
					</div>

					<div class="form-group">
						<label for="image">Image</label>
						<input class="form-control" type="file" name="image" id="image">
					</div>
				</form>
				<div class="float-right">
					<a class="btn btn-warning" href="/assets">Cancel</a>
					<button class="btn btn-primary" id="addCatBtn">Add Asset Line</button>
				</div>
		   	</div>  

		   
		</div>
	</div>
</div>
{{-- <script src="{{asset('js/addCat.js')}}"></script> --}}
@endsection