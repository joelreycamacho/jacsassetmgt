@extends('layouts.app')

@section('content')
	<div class="row">
		<div class="col-12">
			<div class="">
				<div class="card-header p-0 mr-1 ml-1 bg-primary rounded-top">
					@can('isAdmin')
						<h4 class="text-white m-0 p-1 pl-3 bg-dark rounded">Transactions</h4>
					@else
						<h4 class="text-white m-0 p-1 pl-3 bg-dark rounded">My Transactions</h4>
					@endcan
				</div>	
				<div class="card-body pt-0 mt-0 row">
					<div class="col-2 p-2 bg-dark">
						<div class="nav nav-pills">
							<a class="btn btn-outline-primary mb-1 w-100 rounded-left btn-nav active" data-toggle="tab" href="#openTrxns"><span class="ml-3">Open</span>
								<span id="openTrxnsCount" class="badge badge-light bg-primary float-right mr-1 mt-1" >{{$openTrxns->count()}}</span></a>			
							<a class="btn btn-outline-warning mb-1 w-100 rounded-left btn-nav" data-toggle="tab" href="#cancelledTrxns"><span class="ml-3">Cancelled</span>
								<span id="cancelledTrxnsCount" class="badge badge-light bg-warning float-right mr-1 mt-1" >{{$cancelledTrxns->count()}}</span></a>
							<a class="btn btn-outline-danger mb-1 w-100 rounded-left btn-nav" data-toggle="tab"href="#disapprovedTrxns"><span class="ml-3">Disapproved</span>
								<span id="disapprovedTrxnsCount" class="badge badge-light bg-danger float-right mr-1 mt-1" >{{$disapprovedTrxns->count()}}</span></a>
							<a class="btn btn-outline-success mb-1 w-100 rounded-left btn-nav" data-toggle="tab"href="#approvedTrxns"><span class="ml-3">Approved</span>
								<span id="approvedTrxnsCount" class="badge badge-light bg-success float-right mr-1 mt-1" >{{$approvedTrxns->count()}}</span></a>
							<a class="btn btn-outline-info mb-1 w-100 rounded-left btn-nav" data-toggle="tab"href="#returnedTrxns"><span class="ml-3">Returned</span>
								<span id="returnedTrxnsCount" class="badge badge-light bg-info float-right mr-1 mt-1" >{{$returnedTrxns->count()}}</span></a>
						</div>
					</div>
					<div class="col-10 p-2 tab-content bg-dark">
						<div id="trxnNotif" class=""></div>
						<div id="openTrxns" class="tab-pane active">
							<table class="table table-primary table-responsive table-bordered table-sm" >
								<thead>
									<tr class="">	
										<th>Ticket Number</th>
										<th>Asset Line</th>
										<th>Author</th>
										<th>Asset Number</th>
										<th>Deploy Date</th>
										<th>Return Date</th>
										<th>Action</th>
									</tr>
								</thead>
								<tbody id="openTrxnsTbody">	
									@foreach($openTrxns as $openTrxn)
									<tr>
										<td>{{$openTrxn->refNum}}</td>
										<td>{{$openTrxn->category->name}}</td>
										@can('isAdmin')
											<td>{{$openTrxn->user->name}}</td>
										@else
											<td>Me</td>
										@endcan
										@if($openTrxn->asset_id != null)	
											<td>{{$openTrxn->asset->serialNum}}</td>
										@else
											<td></td>
										@endif		
										<td>{{$openTrxn->deployDate}}</td>
										<td>{{$openTrxn->returnDate}}</td>
										<td data-id="{{$openTrxn->id}}">
											@can('isAdmin')
												<a class="btn btn-success rounded p-0 pl-1 pr-1 apprvButton" >Approve</a>
												<a class="btn btn-danger rounded p-0 pl-1 pr-1 disapprvButton" >Disapprove</a>
											@else
												<a class="btn btn-warning rounded p-0 pl-1 pr-1 cnclButton" >Cancel</a>
											@endcan
										</td>
									</tr>
									@endforeach
								</tbody>
							</table>
						</div>
						<div id="cancelledTrxns" class="tab-pane fade">
							<table class="table table-warning table-responsive table-bordered table-sm">
								<thead>
									<tr class="">	
										<th>Ticket Number</th>
										<th>Asset Line</th>
										<th>Author</th>
										<th>Asset Number</th>
										<th>Deploy Date</th>
										<th>Return Date</th>
										<th>Action</th>
									</tr>
								</thead>
								<tbody id="cancelledTrxnsTbody" >	
									@foreach($cancelledTrxns as $cancelledTrxn)
									<tr>
										<td>{{$cancelledTrxn->refNum}}</td>
										<td>{{$cancelledTrxn->category->name}}</td>
										@can('isAdmin')
											<td>{{$cancelledTrxn->user->name}}</td>
										@else
											<td>Me</td>
										@endcan
										@if($cancelledTrxn->asset_id != null)	
											<td>{{$cancelledTrxn->asset->serialNum}}</td>
										@else
											<td></td>
										@endif											
										<td>{{$cancelledTrxn->deployDate}}</td>
										<td>{{$cancelledTrxn->returnDate}}</td>
										<td></td>
									</tr>
									@endforeach
								</tbody>
							</table>
						</div>
						<div id="disapprovedTrxns" class="tab-pane fade">
							<table class="table table-danger table-responsive table-bordered table-sm">
								<thead>
									<tr class="">	
										<th>Ticket Number</th>
										<th>Asset Line</th>
										<th>Author</th>
										<th>Asset Number</th>
										<th>Deploy Date</th>
										<th>Return Date</th>
										<th>Action</th>
									</tr>
								</thead>
								<tbody id="disapprovedTrxnsTbody">	
									@foreach($disapprovedTrxns as $disapprovedTrxn)
										<tr>
											<td>{{$disapprovedTrxn->refNum}}</td>
											<td>{{$disapprovedTrxn->category->name}}</td>
											@can('isAdmin')
												<td>{{$disapprovedTrxn->user->name}}</td>
											@else
												<td>Me</td>
											@endcan
											@if($disapprovedTrxn->asset_id != null)	
												<td>{{$disapprovedTrxn->asset->serialNum}}</td>
											@else
												<td></td>
											@endif	
											<td>{{$disapprovedTrxn->deployDate}}</td>
											<td>{{$disapprovedTrxn->returnDate}}</td>
											<td></td>
										</tr>
									@endforeach
								</tbody>
							</table>
						</div>
						<div id="approvedTrxns" class="tab-pane fade">
							<table class="table table-success table-responsive table-bordered table-sm">
								<thead>
									<tr class="">	
										<th>Ticket Number</th>
										<th>Asset Line</th>
										<th>Author</th>
										<th>Asset Number</th>
										<th>Deploy Date</th>
										<th>Return Date</th>
										<th>Action</th>
									</tr>
								</thead>
								<tbody id="approvedTrxnsTbody">	
									@foreach($approvedTrxns as $approvedTrxn)
									<tr >
										<td>{{$approvedTrxn->refNum}}</td>
										<td>{{$approvedTrxn->category->name}}</td>
										@can('isAdmin')
											<td>{{$approvedTrxn->user->name}}</td>
										@else
											<td>Me</td>
										@endcan
										@if($approvedTrxn->asset_id != null)	
											<td>{{$approvedTrxn->asset->serialNum}}</td>
										@else
											<td></td>
										@endif
										<td>{{$approvedTrxn->deployDate}}</td>
										<td>{{$approvedTrxn->returnDate}}</td>
										<td data-id="{{$approvedTrxn->id}}">
										@can('isAdmin')
											<a class="btn btn-primary rounded p-0 pl-1 pr-1 rtrnButton" >Mark Returned</a>
										@endcannot
										</td>
									</tr>
									@endforeach
								</tbody>
							</table>
						</div>
						<div id="returnedTrxns" class="tab-pane fade">
							<table class="table table-info table-responsive table-bordered table-sm">
								<thead>
									<tr class="">	
										<th>Ticket Number</th>
										<th>Asset Line</th>
										<th>Author</th>
										<th>Asset Number</th>
										<th>Deploy Date</th>
										<th>Return Date</th>
										<th>Action</th>
									</tr>
								</thead>
								<tbody id="returnedTrxnsTbody">	
									@foreach($returnedTrxns as $returnedTrxn)
									<tr>
										<td>{{$returnedTrxn->refNum}}</td>
										<td>{{$returnedTrxn->category->name}}</td>
										@can('isAdmin')
											<td>{{$returnedTrxn->user->name}}</td>
										@else
											<td>Me</td>
										@endcan
										@if($returnedTrxn->asset_id != null)	
											<td>{{$returnedTrxn->asset->serialNum}}</td>
										@else
											<td></td>
										@endif
										<td>{{$returnedTrxn->deployDate}}</td>
										<td>{{$returnedTrxn->returnDate}}</td>
										<td></td>
									</tr>
									@endforeach
								</tbody>
							</table>
						<div>
					</div>
				</div>	
			</div>
		</div>
	</div>
<script src="{{asset('js/trxn.js')}}"></script>
@endsection