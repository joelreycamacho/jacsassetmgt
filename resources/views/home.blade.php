@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-12">
            <div class="card alert-primary">
                <div class="card-header p-0 pt-2 pl-3">
                    <div class="row">
                        <div class="col-6">
                            <h4 class="">Dashboard</h4>
                        </div>
                        <div class="col-6">
                            <h5 class="text-right mr-3">{{$greeting}}</h5>
                        </div>
                    </div>
                </div>
                <div class="card-body">
                    <div class="row">
                        <div class="col-4 text-center">
                            <h3 class="m-0 p-0">Transactions</h3>
                            <div class="text-large ">{{$trxns}}</div>
                        </div>
                        <div class="col-4 text-center">
                            <h3 class="m-0 p-0">Assets</h3>
                            <div class="text-large ">{{$assets}}</div>
                        </div>
                        <div class="col-4 text-center">
                            <h3 class="m-0 p-0">Asset Lines</h3>
                            <div class="text-large ">{{$assetLines}}</div>
                        </div>
                    </div>
                    <hr>
                    <div class="row">
                        <div class="col-6 text-center">
                            <h4 class="m-0 p-0">Open transactions</h4>
                            <div class="text-large ">{{$openTrxns}}</div>
                        </div>
                        <div class="col-6 text-center">
                            <h4 class="m-0 p-0">Available assets</h4>
                            <div class="text-large ">{{$availableAssets}}</div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<style type="">
.text-large {
    font-size: 100px;
    padding: 0;
    margin: 0;
}
</style>
@endsection
