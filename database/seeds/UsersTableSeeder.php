<?php

use Illuminate\Database\Seeder;

class UsersTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {
        

        \DB::table('users')->delete();
        
        \DB::table('users')->insert(array (
            0 => 
            array (
                'id' => 6,
                'name' => 'Admin',
                'email' => 'admin@test.com',
                'email_verified_at' => NULL,
                'password' => '$2y$10$QKjjbweT.kGBDhaUK9DubuLPMrzI7Z7VuhHuVgYO6H./T7j.c4Shu',
                'remember_token' => NULL,
                'created_at' => '2020-04-28 02:19:51',
                'updated_at' => '2020-04-28 02:19:51',
                'role_id' => 1,
            ),
            1 => 
            array (
                'id' => 7,
                'name' => 'Non-admin1',
                'email' => 'non-admin1@test.com',
                'email_verified_at' => NULL,
                'password' => '$2y$10$mKXTvmSwZppac4R2m7jxNuLiNo0D1v0tz78.3QjUJofglI6raOFKm',
                'remember_token' => NULL,
                'created_at' => '2020-04-28 02:20:44',
                'updated_at' => '2020-04-28 02:20:44',
                'role_id' => 2,
            ),
            2 => 
            array (
                'id' => 8,
                'name' => 'Non-admin2',
                'email' => 'non-admin2@test.com',
                'email_verified_at' => NULL,
                'password' => '$2y$10$7iiPKsE6MIZRpejLkHpRaO5f0k8cCA3FkQbwYyjpejmK5s3WoxIV.',
                'remember_token' => NULL,
                'created_at' => '2020-04-28 02:21:15',
                'updated_at' => '2020-04-28 02:21:15',
                'role_id' => 2,
            ),
        ));
        
        
    }
}