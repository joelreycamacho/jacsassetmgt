<?php

use Illuminate\Database\Seeder;

class TransactionsTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {
        

        \DB::table('transactions')->delete();
        
        \DB::table('transactions')->insert(array (
            0 => 
            array (
                'id' => 75,
                'refNum' => '202004281QBN',
                'user_id' => 7,
                'status_id' => 2,
                'category_id' => 15,
                'asset_id' => NULL,
                'deployDate' => '2020-04-30',
                'returnDate' => '2020-05-27',
                'created_at' => '2020-04-28 09:53:50',
                'updated_at' => '2020-04-28 09:54:27',
            ),
            1 => 
            array (
                'id' => 76,
                'refNum' => '20200428ZDVE',
                'user_id' => 7,
                'status_id' => 3,
                'category_id' => 15,
                'asset_id' => NULL,
                'deployDate' => '2020-04-30',
                'returnDate' => '2020-05-27',
                'created_at' => '2020-04-28 09:53:57',
                'updated_at' => '2020-04-28 09:56:02',
            ),
            2 => 
            array (
                'id' => 77,
                'refNum' => '20200428C8TI',
                'user_id' => 7,
                'status_id' => 5,
                'category_id' => 15,
                'asset_id' => 46,
                'deployDate' => '2020-04-30',
                'returnDate' => '2020-05-28',
                'created_at' => '2020-04-28 09:54:14',
                'updated_at' => '2020-04-28 09:57:05',
            ),
        ));
        
        
    }
}