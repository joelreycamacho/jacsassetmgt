<?php

use Illuminate\Database\Seeder;

class CategoriesTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {
        

        \DB::table('categories')->delete();
        
        \DB::table('categories')->insert(array (
            0 => 
            array (
                'id' => 15,
                'name' => 'First Asset Line Test',
                'description' => 'Test Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmodtempor incididunt ut labore et dolore magna aliqua.',
                'img_path' => 'images/1587635859.png',
                'code' => 'XFZ189 Test',
                'isActive' => 1,
                'created_at' => '2020-04-22 12:45:12',
                'updated_at' => '2020-04-28 09:59:34',
            ),
            1 => 
            array (
                'id' => 16,
                'name' => 'Second Asset Line',
                'description' => 'Duis aute irure dolor in reprehenderit in voluptate velit esse
cillum dolore eu fugiat nulla pariatur.',
                'img_path' => 'images/1587827125.png',
                'code' => 'QWT563',
                'isActive' => 1,
                'created_at' => '2020-04-22 13:49:42',
                'updated_at' => '2020-04-28 09:59:38',
            ),
            2 => 
            array (
                'id' => 17,
                'name' => 'Third Asset Line',
                'description' => 'Duis aute irure dolor in reprehenderit in voluptate velit esse
cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non
proident, sunt in culpa qui officia deserunt mollit anim id est laborum.',
                'img_path' => 'images/1587716871.png',
                'code' => 'FDS556',
                'isActive' => 0,
                'created_at' => '2020-04-23 10:27:44',
                'updated_at' => '2020-04-28 09:59:41',
            ),
            3 => 
            array (
                'id' => 18,
                'name' => 'Fourth Asset Line',
                'description' => 'Ut enim ad minim veniam,quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodoconsequat',
                'img_path' => 'images/1587827112.png',
                'code' => 'SDF525',
                'isActive' => 1,
                'created_at' => '2020-04-24 03:20:03',
                'updated_at' => '2020-04-25 15:05:12',
            ),
            4 => 
            array (
                'id' => 19,
                'name' => 'Fifth Asset Line',
                'description' => 'Excepteur sint occaecat cupidatat non
proident, sunt in culpa qui officia deserunt mollit anim id est laborum',
                'img_path' => 'images/1587708209.png',
                'code' => 'LKI425',
                'isActive' => 1,
                'created_at' => '2020-04-24 06:03:29',
                'updated_at' => '2020-04-25 15:00:17',
            ),
            5 => 
            array (
                'id' => 20,
                'name' => 'Sixth Asset Line',
                'description' => 'Description of Sixth Asset Line',
                'img_path' => 'images/1587708253.png',
                'code' => 'KKO789',
                'isActive' => 1,
                'created_at' => '2020-04-24 06:04:13',
                'updated_at' => '2020-04-28 09:39:42',
            ),
        ));
        
        
    }
}