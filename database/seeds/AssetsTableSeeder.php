<?php

use Illuminate\Database\Seeder;

class AssetsTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {
        

        \DB::table('assets')->delete();
        
        \DB::table('assets')->insert(array (
            0 => 
            array (
                'id' => 45,
                'serialNum' => '1587608710',
                'isAvailable' => 1,
                'created_at' => '2020-04-23 02:25:10',
                'updated_at' => '2020-04-28 09:59:33',
                'category_id' => 15,
            ),
            1 => 
            array (
                'id' => 46,
                'serialNum' => '1587608711',
                'isAvailable' => 1,
                'created_at' => '2020-04-23 02:25:10',
                'updated_at' => '2020-04-28 09:59:33',
                'category_id' => 15,
            ),
            2 => 
            array (
                'id' => 47,
                'serialNum' => '1587608712',
                'isAvailable' => 1,
                'created_at' => '2020-04-23 02:25:10',
                'updated_at' => '2020-04-28 09:59:34',
                'category_id' => 15,
            ),
            3 => 
            array (
                'id' => 48,
                'serialNum' => '1587637368',
                'isAvailable' => 1,
                'created_at' => '2020-04-23 10:22:48',
                'updated_at' => '2020-04-28 09:59:38',
                'category_id' => 16,
            ),
            4 => 
            array (
                'id' => 49,
                'serialNum' => '1587637369',
                'isAvailable' => 1,
                'created_at' => '2020-04-23 10:22:48',
                'updated_at' => '2020-04-28 09:59:38',
                'category_id' => 16,
            ),
            5 => 
            array (
                'id' => 50,
                'serialNum' => '1587698416',
                'isAvailable' => 0,
                'created_at' => '2020-04-24 03:20:16',
                'updated_at' => '2020-04-28 02:53:08',
                'category_id' => 18,
            ),
            6 => 
            array (
                'id' => 51,
                'serialNum' => '1587698417',
                'isAvailable' => 0,
                'created_at' => '2020-04-24 03:20:16',
                'updated_at' => '2020-04-28 08:28:41',
                'category_id' => 18,
            ),
            7 => 
            array (
                'id' => 52,
                'serialNum' => '1588042959',
                'isAvailable' => 1,
                'created_at' => '2020-04-28 03:02:39',
                'updated_at' => '2020-04-28 09:59:34',
                'category_id' => 15,
            ),
            8 => 
            array (
                'id' => 53,
                'serialNum' => '1588043069',
                'isAvailable' => 1,
                'created_at' => '2020-04-28 03:04:29',
                'updated_at' => '2020-04-28 09:59:38',
                'category_id' => 16,
            ),
            9 => 
            array (
                'id' => 54,
                'serialNum' => '1588043070',
                'isAvailable' => 1,
                'created_at' => '2020-04-28 03:04:29',
                'updated_at' => '2020-04-28 09:59:38',
                'category_id' => 16,
            ),
            10 => 
            array (
                'id' => 55,
                'serialNum' => '1588053757',
                'isAvailable' => 0,
                'created_at' => '2020-04-28 06:02:37',
                'updated_at' => '2020-04-28 06:09:39',
                'category_id' => 20,
            ),
            11 => 
            array (
                'id' => 56,
                'serialNum' => '1588053758',
                'isAvailable' => 0,
                'created_at' => '2020-04-28 06:02:37',
                'updated_at' => '2020-04-28 07:58:58',
                'category_id' => 20,
            ),
            12 => 
            array (
                'id' => 57,
                'serialNum' => '1588053759',
                'isAvailable' => 0,
                'created_at' => '2020-04-28 06:02:37',
                'updated_at' => '2020-04-28 08:47:29',
                'category_id' => 20,
            ),
            13 => 
            array (
                'id' => 58,
                'serialNum' => '1588053760',
                'isAvailable' => 0,
                'created_at' => '2020-04-28 06:02:37',
                'updated_at' => '2020-04-28 09:26:56',
                'category_id' => 20,
            ),
        ));
        
        
    }
}