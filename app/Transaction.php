<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Transaction extends Model
{
    public function asset()
    {
    	return $this->belongsTo('\App\Asset');
    }

    public function category()
    {
    	return $this->belongsTo('\App\Category');
    }

    public function user()
    {
    	return $this->belongsTo('\App\User');
    }

    public function status()
    {
    	return $this->belongsTo('\App\Status');
    }
}
