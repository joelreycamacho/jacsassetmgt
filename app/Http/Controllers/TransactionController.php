<?php

namespace App\Http\Controllers;

use App\Transaction;
use App\Category;
use App\User;
use App\Status;
use App\Asset;
use Auth;
use Illuminate\Http\Request;
use Illuminate\Support\Str;

class TransactionController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        
        $this->authorize('view', Transaction::class);

        if (Auth::user()->role_id == 1) {
            $openTrxns = Transaction::where('status_id',1)->get();
            $cancelledTrxns = Transaction::where('status_id',2)->get();
            $disapprovedTrxns = Transaction::where('status_id',3)->get();
            $approvedTrxns = Transaction::where('status_id',4)->get();
            $returnedTrxns = Transaction::where('status_id',5)->get();
        } else {
            $user_id = Auth::user()->id;
            $openTrxns = Transaction::where('user_id',$user_id)->where('status_id',1)->get();                          
            $cancelledTrxns = Transaction::where('user_id',$user_id)->where('status_id',2)->get();                     
            $disapprovedTrxns = Transaction::where('user_id',$user_id)->where('status_id',3)->get();                 
            $approvedTrxns = Transaction::where('user_id',$user_id)-> where('status_id',4)->get();                 
            $returnedTrxns = Transaction::where('user_id',$user_id)->where('status_id',5)->get();   
                                               
        }

        $users = User::all();
        $status = Status::all();
        $categories = Category::all();
        $assets = Asset::all();
        return view('transactions.index')   
            ->with('openTrxns', $openTrxns)
            ->with('cancelledTrxns', $cancelledTrxns)
            ->with('disapprovedTrxns', $disapprovedTrxns)
            ->with('approvedTrxns', $approvedTrxns)
            ->with('returnedTrxns', $returnedTrxns)
            ->with('users', $users)
            ->with('categories', $categories)
            ->with('statuses', $status)
            ->with('assets', $assets)
            ;
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Transaction $transaction)
    {              

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        $this->authorize('create', Transaction::class);

        $request->validate([
            'deployDate' => 'required|date|after:tomorrow',
            'returnDate' => 'required|date|after:deployDate'
        ]);
        $refNum = date('Ymd');
        $refNum = $refNum . strToUpper(Str::random(4));
        $category_id = $request->input('category_id');
        $deployDate = $request->input('deployDate');
        $returnDate = $request->input('returnDate');

        $transaction = new Transaction;
        $transaction->refNum = $refNum;
        $transaction->user_id = Auth::user()->id;
        $transaction->status_id = 1;
        $transaction->category_id = $category_id;
        $transaction->deployDate = $deployDate;
        $transaction->returnDate = $returnDate;

        if($transaction->save()){
        $request->session()->flash('status', "Your request has been submitted. Please check the Transactions tab to monitor its progress.");
        }

        return redirect('/categories');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Transaction  $transaction
     * @return \Illuminate\Http\Response
     */
    public function show(Transaction $transaction)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Transaction  $transaction
     * @return \Illuminate\Http\Response
     */
    public function edit(Transaction $transaction)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Transaction  $transaction
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Transaction $transaction) //
    {
        $status_id = $request->input('status_id');

        // return response()->json([
        //     'message' => "Test",
        //     'data' => $status_id
        // ], 200);
    
        $data_id = $transaction->id;
        $category_name = $transaction->category->name;
        $category_id = $transaction->category->id;
        $action = "";
        $transactionUser = "Me";
        $assetNumber = "";

        if (Auth::user()->role_id == 1) {
            $transactionUser = $transaction->user->name;
        } 

        if ($status_id == 5) {
            $assetNumber = $transaction->asset->serialNum;
            $asset = Asset::where('id', $transaction->asset->id)->first();
            $asset->isAvailable= 1;
            $asset->save();
            $message = "Ticket number {$transaction->refNum} has been marked returned.";
        } elseif ($status_id == 4) {
            $asset = Asset::where('category_id', $category_id)->where('isAvailable', 1)->first();
            if ($asset != null) {
                $assetId = $asset->id;
                $assetNumber = $asset->serialNum;
                $transaction->asset_id = $assetId;
                $asset = Asset::where('id', $assetId)->first(); 
                $asset->isAvailable= 0;
                $asset->save();
                $message = "Ticket {$transaction->refNum} has been approved.";
                $action = "<a 
                    class=\"btn btn-primary rounded p-0 pl-1 pr-1 rtrnButton\" >Mark Returned</a>";              
            } else {
                return response()->json([
                'message' => "No asset available for the request."
                ], 500);
            }
        } elseif ($status_id == 3){
            $message = "Ticket {$transaction->refNum} has been disapproved.";
        } elseif($status_id == 2){
            $message = "Ticket {$transaction->refNum} has been cancelled.";
        } else {
            $assetNumber = "";
        }

        $transaction->status_id = $status_id;
        if($transaction->save()){
            return response()->json([
            'message' => "{$message}",
            //the data property of our response contains the HTML element that we want to append to our <select> element in the products.create view
            'data' => 
                    "<tr >
                        <td>$transaction->refNum</td>
                        <td>$category_name</td>
                        <td>$transactionUser</td>
                        <td>$assetNumber</td>
                        <td>$transaction->deployDate</td>
                        <td>$transaction->returnDate</td>
                        <td data-id=\"$data_id\">$action</td>
                    </tr>"
            ], 201);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Transaction  $transaction
     * @return \Illuminate\Http\Response
     */
    public function destroy(Transaction $transaction)
    {
        return "Test message destroy";
    }
}
