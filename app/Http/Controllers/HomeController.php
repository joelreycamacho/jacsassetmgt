<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Transaction;
use App\Category;
use App\User;
use App\Status;
use App\Asset;
use Auth;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        // return view('home');
        if (Auth::user()->role_id == 1) {
            $openTrxns = Transaction::where('status_id',1)->count();
            $trxns = Transaction::All()->count();
            $assets = Asset::All()->count();
            $assetLines = Category::All()->count();
            $availableAssets = Asset::where('isAvailable',1)->count();
            $name = Auth::user()->name;
            date_default_timezone_set('asia/manila');
            $greeting = (date('H') - 0);
            if ($greeting > 17) {
                $greeting = "Good evening, " . $name . "!";
            } elseif($greeting > 11) {
                $greeting = "Good afternoon, " . $name . "!";
            } else {
                $greeting = "Good morning, " . $name . "!";
            }
    
            return view('home')
                ->with('greeting', $greeting)
                ->with('openTrxns', $openTrxns)
                ->with('trxns', $trxns)
                ->with('assets', $assets)
                ->with('assetLines', $assetLines)
                ->with('availableAssets', $availableAssets);

        } else {
            return redirect('/transactions');
        }
        
    }
}
