<?php

namespace App\Http\Controllers;

use App\Category;
use App\Asset;
use App\Transaction;
use Illuminate\Http\Request;

class CategoryController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $this->authorize('view', Category::class);

        date_default_timezone_set('asia/manila');
        $date = date_create(date('Y-m-d'));
        $date =date_add($date, date_interval_create_from_date_string('2 days'));
        $minDate = date_format($date, 'Y-m-d');
        $date =date_add($date, date_interval_create_from_date_string('90 days'));
        $maxDate = date_format($date, 'Y-m-d');

        $categories = Category::all();
        $assets = Asset::all();
        return view('categories.index')
            ->with('categories', $categories)
            ->with('assets', $assets)
            ->with('minDate', $minDate)
            ->with('maxDate', $maxDate);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $this->authorize('create', Category::class);
        $categories = Category::all();
        return view('categories.create')->with('categories', $categories);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //implement authorization via Laravel Policy
        $this->authorize('create', Category::class);

        //retrieve the property named category from the FormData object sent via JS Fetch and save it as a variable named $name
        $name = $request->input('name');
        $description = $request->input('description');
        $code = $request->input('code');
        $image = $request->input('image');
        // return response()->json([
        //     'message' => "My fetch works." . $image,
        //     'data' => $name . "," . $description . "," . $code
        // ], 200);
        //validate if a category name was received
        if(isset($name)){
            //check if data type is correct and is not empty
            if( gettype($name) === "string" && $name != "" &&
                gettype($description) === "string" && $description != "" &&
                gettype($code) === "string" && $code != "" 
                // gettype($image) === "string" && $image != ""
            ){
                //selectively query for a category whose name field matches our received $name value, only get the first result
                $duplicate = Category::where(strtolower('name'), strtolower($name))->first();
                //if no duplicates found
                if($duplicate === null){
                    //sanitize our input
                    $cleanName = htmlspecialchars($name);
                    $cleanDescription = htmlspecialchars($description);
                    $cleanCode = htmlspecialchars($code);
                    $image = $request->file('image');
                    //handle the file image upload
                    //set the file name of the uploaded image to be the time of upload, retaining the original file type
                    $file_name = time() . "." . $image->getClientOriginalExtension();
                    //set target destination where the file will be saved in
                    $destination = "images/";
                    //call the move() method of the $image object to save the uploaded file in the target destination under the specified file name
                    $image->move($destination, $file_name);
                    //set the path of the saved image as the value for the column img_path of this record

                    //instantiate a new Category object from the Category model
                    $category = new Category;
                    //set the value of this category's name to be the sanitized form input
                    $category->name = $cleanName;
                    $category->description = $cleanDescription;
                    $category->code = $cleanCode;

                    
                    $category->img_path = $destination.$file_name;

                    //if successfully saved
                    if($category->save()){
                        return response()->json([
                            'message' => "{$cleanName} added successfully.",
                            //the data property of our response contains the HTML element that we want to append to our <select> element in the products.create view
                            'data' => "<option value=\"$category->id\">$category->name</option>"
                        ], 201);//successful creation of a resource is denoted by an HTTP status code of 201
                    }else{//failed to save the new $category
                        return response()->json([
                            'message' => "Failed to save new category.",
                        ], 500);
                    }
                }else{//duplicate found
                    return response()->json([
                        'message' => "Category already exists."
                    ], 403);
                }
            }else{//submitted name is either not a string or is an empty string
                return response()->json([
                    'message' => "All fields are required and it has to be a string."
                ], 403);//403 stands for forbidden action, this is different from 401 which is unauthorized
            }
        }else{//no category name was received from JS Fetch in the create.blade.php view
            return response()->json([
                'message' => "No category name was received."
            ], 500);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Category  $category
     * @return \Illuminate\Http\Response
     */
    public function show(Category $category)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Category  $category
     * @return \Illuminate\Http\Response
     */
    public function edit(Category $category)
    {
        //dd($category);
        $this->authorize('update', Category::class);
        return view('categories.edit')->with('category', $category);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Category  $category
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Category $category)
    {
        //dd($category->name);
        $this->authorize('update', Category::class);

        $request->validate([
            'name' => 'required|string',
            'description' => 'required|string',
            'code' => 'required',
            'image' => 'image'
        ]);

        $name = htmlspecialchars($request->input('name'));
        $description = htmlspecialchars($request->input('description'));
        $code = htmlspecialchars($request->input('code'));
        $image = $request->file('image');

        //$asset is the asset object to be edited, this was obtained via Laravel's route-model binding
        //overwrite the properties of $asset with the input values from the edit form
        $category->name = $name;
        $category->description = $description;
        $category->code = $code;

        //if an image file upload is found, replace the current image of the asset with the new upload
        if($request->file('image') != null){
            //handle the file image upload
            //set the file name of the uploaded image to be the time of upload, retaining the original file type
            $file_name = time() . "." . $image->getClientOriginalExtension();
            //set target destination where the file will be saved in
            $destination = "images/";
            //call the move() method of the $image object to save the uploaded file in the target destination under the specified file name
            $image->move($destination, $file_name);
            //set the path of the saved image as the value for the column img_path of this record
            $category->img_path = $destination.$file_name;
        }

        $category->save();

        return redirect("/categories");
    }
    

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Category  $category
     * @return \Illuminate\Http\Response
     */
    public function destroy(Category $category)
    {
        $assets = Asset::where('category_id', $category->id)->get();
        $transactions = Transaction::where('category_id', $category->id)->where('status_id', 4)->get();
        
        if($category->isActive == 1){
            $category->isActive = 0;
            foreach ($assets as $asset) {
                    $asset->isAvailable = 0;
                    $asset->save();
                }    
        }else{
            $category->isActive = 1;
            foreach ($assets as $asset) {
                if ($transactions->count() > 0) {
                    foreach ($transactions as $transaction) {
                        if($transaction->asset_id != $asset->id) {
                            $asset->isAvailable = 1;
                            $asset->save();
                        } 
                    }
                } else {
                    $asset->isAvailable = 1;
                    $asset->save();
                }
            } 
        }                
        $category->save();
        return redirect('/categories');
    }
}
