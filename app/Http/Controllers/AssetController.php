<?php

namespace App\Http\Controllers;

use App\Asset;
use App\Category;
use Illuminate\Http\Request;

class AssetController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $this->authorize('view', Asset::class);
        $categories = Category::all();
        $assets = Asset::all();
        return view('assets.index')->with('assets', $assets)->with('categories', $categories);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //use the authorize() controller helper to check the create() action of the assetPolicy class
        //if authorization fails the check, we expect an HTTP response of status code 403
        $this->authorize('create', Asset::class);
        $categories = Category::all();
        return view('assets.create')->with('categories', $categories);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //dd($request->input('name'));
        $this->authorize('create', Asset::class);

        //call the validate() method on our request to apply validation rules on specified request input
        $request->validate([
            'quantity' => 'required|numeric',
            'category' => 'required|numeric'
        ]);

        $quantity = htmlspecialchars($request->input('quantity'));
        $category = htmlspecialchars($request->input('category'));

        //instantiate a new asset object from the asset model
        
        //set the properties of this object as defined in its migration to their respective sanitized form input values
        for ($i=0; $i < $quantity ; $i++) { 
            $asset = new Asset;
            $asset->serialNum = time() + $i;
            $asset->category_id = $category;
            $asset->save();
        }

        

        //save the new asset object as a new record in the assets table via its save() method
       
        //this will now call the index action of the assetController as per Laravel's resourceful routes
        return redirect('/assets'); 
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\asset  $asset
     * @return \Illuminate\Http\Response
     */
    public function show(asset $asset)
    {
        return view('assets.show')->with('asset', $asset);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\asset  $asset
     * @return \Illuminate\Http\Response
     */
    public function edit(asset $asset)
    {
        //die-dump the name of the asset to be edited
        //this works due to route-model binding
        //dd($asset->name);
        //use the authorize() controller helper to call the update method in the assetPolicy

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\asset  $asset
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, asset $asset)
    {

    }
    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\asset  $asset
     * @return \Illuminate\Http\Response
     */
    public function destroy(asset $asset)
    {

    }
}
