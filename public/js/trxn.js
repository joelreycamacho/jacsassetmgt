const csrfToken = document.querySelector('meta[name="csrf-token"]').getAttribute('content');

let openTrxnsCount = document.getElementById('openTrxnsCount')
let openTrxnsTbody = document.getElementById('openTrxnsTbody')
let cancelledTrxnsCount = document.getElementById('cancelledTrxnsCount')
let cancelledTrxnsTbody = document.getElementById('cancelledTrxnsTbody')
let disapprovedTrxnsCount = document.getElementById('disapprovedTrxnsCount')
let disapprovedTrxnsTbody = document.getElementById('disapprovedTrxnsTbody')
let approvedTrxnsCount = document.getElementById('approvedTrxnsCount')
let approvedTrxnsTbody = document.getElementById('approvedTrxnsTbody')
let returnedTrxnsCount = document.getElementById('returnedTrxnsCount')
let returnedTrxnsTbody = document.getElementById('returnedTrxnsTbody')

// document.querySelectorAll('.cnclButton').forEach((btn)=> {
document.querySelectorAll('.cnclButton').forEach((btn)=> {
	btn.addEventListener('click', ()=>{
		let trxn_id = btn.parentElement.getAttribute('data-id');
		let row = btn.parentElement.parentElement;
		// let	status_id = 2;
		// let formData = new FormData;
		// formData.append('status_id', status_id);

		fetch(`/transactions/${trxn_id}`, {
		    method: "PATCH",
		    body: JSON.stringify({
		    	"status_id": 2
		    }),
		    // put the CSRF token in the request header
		    headers: {
		        "X-CSRF-TOKEN": csrfToken,
		        "Content-Type": "application/json"
		    }
		})
		// when the promise object returned by our fetch request resolves, execute the anonymous function passed in to the then() method:
		.then((res)=>{
		    // 201 status code signifies successful creation of our category
		    if(res.status === 201){
		        trxnNotif.setAttribute('class', 'alert alert-warning mb-1 p-2');
		    }else{
		        trxnNotif.setAttribute('class', 'alert alert-danger mb-1 p-2');
		    }
		    return res.json();
		})
		.then((data)=>{
			// console.log(data.message);
			// console.log(data.data);
		    trxnNotif.innerHTML = data.message;
		    cancelledTrxnsTbody.innerHTML += data.data;
		    openTrxnsCount.innerHTML = parseInt(openTrxnsCount.innerHTML) - 1;
		    cancelledTrxnsCount.innerHTML = parseInt(cancelledTrxnsCount.innerHTML) + 1;
		    openTrxnsTbody.removeChild(row);
		})
	})
})

document.querySelectorAll('.disapprvButton').forEach((btn)=> {
	btn.addEventListener('click', ()=>{
		let trxn_id = btn.parentElement.getAttribute('data-id');
		let row = btn.parentElement.parentElement;
		fetch(`/transactions/${trxn_id}`, {
		    method: "PATCH",
		    body: JSON.stringify({
		    	"status_id": 3
		    }),
		    // put the CSRF token in the request header
		    headers: {
		        "X-CSRF-TOKEN": csrfToken,
		        "Content-Type": "application/json"
		    }
		})
		// when the promise object returned by our fetch request resolves, execute the anonymous function passed in to the then() method:
		.then((res)=>{
		    if(res.status === 201){
		        trxnNotif.setAttribute('class', 'alert alert-danger mb-1 p-2');	        
		    }else{
		    	trxnNotif.setAttribute('class', 'alert alert-danger mb-1 p-2');
		    }
		    return res.json();		   
		})
		.then((data)=>{
			// console.log(data.message);
			// console.log(data.data);			
		    trxnNotif.innerHTML = data.message;
	    	disapprovedTrxnsTbody.innerHTML += data.data;
	    	openTrxnsCount.innerHTML = parseInt(openTrxnsCount.innerHTML) - 1;
	    	disapprovedTrxnsCount.innerHTML = parseInt(disapprovedTrxnsCount.innerHTML) + 1;
	    	openTrxnsTbody.removeChild(row);
		})

	})
})

document.querySelectorAll('.apprvButton').forEach((btn)=> {
	btn.addEventListener('click', ()=>{
		let trxn_id = btn.parentElement.getAttribute('data-id');
		let row = btn.parentElement.parentElement;
		fetch(`/transactions/${trxn_id}`, {
		    method: "PUT",
		    body: JSON.stringify({
		    	"status_id": 4
		    }),
		    // put the CSRF token in the request header
		    headers: {
		        "X-CSRF-TOKEN": csrfToken,
		        "Content-Type": "application/json"
		    }
		})
		// when the promise object returned by our fetch request resolves, execute the anonymous function passed in to the then() method:
		.then((res)=>{
		    if(res.status === 201){
		        trxnNotif.setAttribute('class', 'alert alert-success mb-1 p-2');	        
		    }else{
		    	trxnNotif.setAttribute('class', 'alert alert-danger mb-1 p-2');
		    }
		    return res.json();		   
		})
		.then((data)=>{
			// console.log(data.message);
			// console.log(data.data);	
			let message = data.message
		    trxnNotif.innerHTML = message;
		    if (message != "No asset available for the request.") {
		    	approvedTrxnsTbody.innerHTML += data.data;
		    	openTrxnsCount.innerHTML = parseInt(openTrxnsCount.innerHTML) - 1;
		    	approvedTrxnsCount.innerHTML = parseInt(approvedTrxnsCount.innerHTML) + 1;
		    	openTrxnsTbody.removeChild(row);
		    }
		})
	})
})


// document.querySelectorAll('.rtrnButton').forEach((btn)=> {
// 	btn.addEventListener('click', ()=>{
// 		let trxn_id = btn.parentElement.getAttribute('data-id');
// 		let row = btn.parentElement.parentElement;
// 		fetch(`/transactions/${trxn_id}`, {
// 		    method: "PUT",
// 		    body: JSON.stringify({
// 		    	"status_id": 5
// 		    }),
// 		    // put the CSRF token in the request header
// 		    headers: {
// 		        "X-CSRF-TOKEN": csrfToken,
// 		        "Content-Type": "application/json"
// 		    }
// 		})
// 		// when the promise object returned by our fetch request resolves, execute the anonymous function passed in to the then() method:
// 		.then((res)=>{
// 		    if(res.status === 201){
// 		        trxnNotif.setAttribute('class', 'alert alert-primary mb-1 p-2');	        
// 		    }else{
// 		    	trxnNotif.setAttribute('class', 'alert alert-danger mb-1 p-2');
// 		    }
// 		    return res.json();		   
// 		})
// 		.then((data)=>{
// 			// console.log(data.message);
// 			// console.log(data.data);			
// 		    trxnNotif.innerHTML = data.message;
// 		    returnedTrxnsTbody.innerHTML += data.data;
// 	    	returnedTrxnsCount.innerHTML = parseInt(returnedTrxnsCount.innerHTML) + 1;
// 	    	approvedTrxnsCount.innerHTML = parseInt(approvedTrxnsCount.innerHTML) - 1;
// 	    	approvedTrxnsTbody.removeChild(row);
// 		})
// 	})
// })

approvedTrxnsTbody.addEventListener('click', (e)=>{
	if (e.target.classList.contains('rtrnButton')) {
		let trxn_id = e.target.parentElement.getAttribute('data-id');
		let row = e.target.parentElement.parentElement;
		// console.log(trxn_id)
		fetch(`/transactions/${trxn_id}`, {
		    method: "PUT",
		    body: JSON.stringify({
		    	"status_id": 5
		    }),
		    // put the CSRF token in the request header
		    headers: {
		        "X-CSRF-TOKEN": csrfToken,
		        "Content-Type": "application/json"
		    }
		})
		// when the promise object returned by our fetch request resolves, execute the anonymous function passed in to the then() method:
		.then((res)=>{
		    if(res.status === 201){
		        trxnNotif.setAttribute('class', 'alert alert-primary mb-1 p-2');	        
		    }else{
		    	trxnNotif.setAttribute('class', 'alert alert-danger mb-1 p-2');
		    }
		    return res.json();		   
		})
		.then((data)=>{
			console.log(data.message);
			console.log(data.data);			
		    trxnNotif.innerHTML = data.message;
		    returnedTrxnsTbody.innerHTML += data.data;
	    	returnedTrxnsCount.innerHTML = parseInt(returnedTrxnsCount.innerHTML) + 1;
	    	approvedTrxnsCount.innerHTML = parseInt(approvedTrxnsCount.innerHTML) - 1;
	    	approvedTrxnsTbody.removeChild(row);
		})
	}
})