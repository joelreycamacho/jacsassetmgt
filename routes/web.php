<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

//define a resourceful route for the Category resource
//this single resourceful route encompasses all the actions stored in a resourceful controller
//the resource() static method of the Route class needs two parameters:
	//the resource name in plural form
	//the name of the resourceful controller
Route::resource('categories', 'CategoryController');
Route::resource('assets', 'AssetController');
Route::resource('transactions', 'TransactionController');

//define a route for creating a cart item
Route::post('/cart', 'CartController@store');
//define a route for viewing the cart page
Route::get('/cart', 'CartController@index');
//define a route for updating a given product's cart quantity
Route::put('/cart/{id}', 'CartController@update');
//define a route for deleting a given product from the session cart variable
Route::delete('/cart/{id}', 'CartController@destroy');